<?php

namespace Tests\Unit\Movie;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use DiDom\Document;
use App\Movie\Movie;

class MovieTest extends TestCase
{

    /** @var App\Movie\Movie */
    private $movie;

    public function setUp() {
        parent::setUp();

        $doc = new Document(__DIR__ . '/movie.html', true);
        $this->movie = new Movie($doc);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTitle()
    {
        $title = new \App\Movie\Info\Title\Title();
        $this->movie->accept($title);
        $this->assertEquals('Castaway on the Moon', $this->movie->title);
    }

    public function testDirector()
    {
        $director = new \App\Movie\Info\Director\Director();
        $this->movie->accept($director);
        $this->assertEquals('Hae-jun Lee', $this->movie->director);
    }

    public function testYear()
    {
        $year = new \App\Movie\Info\Year\Year();
        $this->movie->accept($year);
        $this->assertSame(2009, $this->movie->year);
    }
}
