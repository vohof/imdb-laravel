<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie\MovieFactory;

class MovieController extends Controller
{
    /** @var \App\Movie\MovieFactory */
    private $factory;

    public function __construct(MovieFactory $factory)
    {
        $this->factory = $factory;
    }

    public function __invoke(Request $req, string $searchTerm)
    {
        try {
            $movie = $this->factory->create($searchTerm);

            return response()->json($movie);
        } catch (\RuntimeException $e) {
            return response()->json('No movie found for searchTerm: ' . $searchTerm, 404);
        }
    }
}
