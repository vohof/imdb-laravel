<?php

namespace App\Movie;

use App\Movie\Movie;
use DiDom\Document;

class MovieFactory
{
    const IMDB_URI = 'https://www.imdb.com/title/{{id}}';

    public function create(string $searchTerm)
    {
        if (preg_match('/^(tt)?\d+$/', $searchTerm)) {
            $url = str_replace('{{id}}', $searchTerm, self::IMDB_URI);
            $movie = new Movie(new Document($url, true));
        } elseif ( preg_match('/^\d+$/', $searchTerm)) {
            $url = str_replace('{{id}}', 'tt' . $searchTerm, self::IMDB_URI);
            $movie = new Movie(new Document($url, true));
        } else {
            $movie = $this->createFromSearch($searchTerm);
        }

        foreach ($this->getVisitors() as $visitor)
        {
            $movie->accept($visitor);
        }

        return $movie;
    }

    public function createFromSearch($searchTerm)
    {

    }

    public function getVisitors()
    {
        return [
            new Info\Title\Title(),
            new Info\Director\Director(),
            new Info\Year\Year(),
        ];
    }
}
