<?php

namespace App\Movie;

use DiDom\Document;
use App\Movie\Info\VisitorInterface;

class Movie implements Visitee
{
    use TurnsMovieToJson;

    /** @var \DiDom\Document */
    private $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    public function getDocument()
    {
        return $this->document;
    }

    public function accept(VisitorInterface $visitor)
    {
        $visitor->visit($this);
    }

    public function __set($key, $value)
    {
        $this->properties[$key] = $value;
    }

    public function __get($key)
    {
        if (isset($this->properties[$key])) {
            return $this->properties[$key];
        }
    }
}
