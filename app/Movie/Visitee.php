<?php

namespace App\Movie;

use App\Movie\Info\VisitorInterface;

interface Visitee
{
    public function accept(VisitorInterface $visitor);
}
