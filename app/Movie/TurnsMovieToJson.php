<?php

namespace App\Movie;

trait TurnsMovieToJson
{
    protected $properties = [];

    public function toJson($options = 0)
    {
        return json_encode($this->properties);
    }
}
