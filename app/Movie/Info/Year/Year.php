<?php

namespace App\Movie\Info\Year;

use App\Movie\Info\Handler;
use App\Movie\Movie;

class Year extends Handler
{
    public function handle(Movie $movie)
    {
        if (count($year = $movie->getDocument()->find('#titleYear')) === 0) {
            return false;
        }

        $movie->year = (int)str_replace(['(',')'], '', $year[0]->text());
    }
}
