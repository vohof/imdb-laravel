<?php

namespace App\Movie\Info;

use App\Movie\Movie;

interface VisitorInterface
{
    public function visit(Movie $movie);
}
