<?php

namespace App\Movie\Info\Title;

use App\Movie\Info\Handler;
use App\Movie\Movie;

class Title extends Handler
{
    public function handle(Movie $movie)
    {
        if (count($title = $movie->getDocument()->find('#ratingWidget strong')) === 0) {
            return false;
        }

        $movie->title = $title[0]->text();
    }
}
