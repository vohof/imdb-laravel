<?php

namespace App\Movie\Info\Director;

use App\Movie\Info\Handler;
use App\Movie\Movie;

class Director extends Handler
{
    public function handle(Movie $movie)
    {
        if (count($director = $movie->getDocument()->find('[itemprop=director] [itemprop=name]')) === 0) {
            return false;
        }

        $movie->director = $director[0]->text();
    }
}
