<?php

namespace App\Movie\Info;

use App\Movie\Movie;

abstract class Handler implements VisitorInterface
{
    /** @var \App\Movie\Handler */
    private $sucessor;

    public function __construct(Handler $successor = null)
    {
        $this->sucessor = $successor;
    }

    public function visit(Movie $movie)
    {
        $handled = $this->handle($movie);
        if (!$handled) {
            if ($this->sucessor !== null) {
                $this->sucessor->visit($movie);
            }
        }
    }

    abstract public function handle(Movie $movie);
}
